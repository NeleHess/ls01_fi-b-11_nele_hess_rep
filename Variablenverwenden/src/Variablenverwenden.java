module Variablen {
	package eingabeAusgabe;

	/** Variablen.java
	    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
	    @author
	    @version
	*/
	public class Variablenverwenden {
	  public static void main(String [] args){
	    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
	          Vereinbaren Sie eine geeignete Variable */
				int zaehler = 0;

	    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
	          und geben Sie ihn auf dem Bildschirm aus.*/
				zaehler = 25;
				System.out.ptinln("Anzahl der Programmdurchläufe:" zaehler);
	    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
	          eines Programms ausgewaehlt werden.
	          Vereinbaren Sie eine geeignete Variable */
				char menuepunkt;
	    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
	          und geben Sie ihn auf dem Bildschirm aus.*/
				menuepunkt = 'C';
				System.out.println("Ihre Eingabe lautet:" menuepunkt);
	    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
	          notwendig.
	          Vereinbaren Sie eine geeignete Variable */
				long astronomischeBerechnung;
	    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
	          und geben Sie sie auf dem Bildschirm aus.*/
				astronomischeBerechnung = 299792458;
				Sytem.out.println("Die Lichtgeschwindigkeit im luftleeren Raum beträgt:" astronomischeBerechnung);
	    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
	          soll die Anzahl der Mitglieder erfasst werden.
	          Vereinbaren Sie eine geeignete Variable und initialisieren sie
	          diese sinnvoll.*/
				byte anzahlMitglieder = 7;
	    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
				System.out.println("Anzahl der Mitglieder:" + anzahlMitglieder);
	    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
	          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
	          dem Bildschirm aus.*/
				float ladung = 0,00000000000000001602f;
				System.out.println("Die elektrische Elementarladung ist:" + ladung);
				System.out.println("As");
	    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
	          Vereinbaren Sie eine geeignete Variable. */
				boolean zahlungseingang;

	    /*11. Die Zahlung ist erfolgt.
	          Weisen Sie der Variable den entsprechenden Wert zu
	          und geben Sie die Variable auf dem Bildschirm aus.*/
				zahlungseingang = true;
				System.out.println("Status des Zahlungseingangs:" + zahlungseingang);

	  }//main
	}// Variablen
}