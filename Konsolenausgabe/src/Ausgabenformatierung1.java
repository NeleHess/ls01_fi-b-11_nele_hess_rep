
public class Ausgabenformatierung1 {

	public static void main(String[] args) {
		System.out.println("Hello Wie geht es dir?");
		System.out.println("Er sagte: \"Guten Tag\"");
		
		String z = "Java-Programm";
		int n = 123;
		double k = 1234.5678;
		double r = 22.4234234;
		double e = 111.2222;
		double l = 4.0;
		double u = 1000000.551;
		double p = 97.34; 

		
		System.out.printf( "|%-18.4s|\n", z );
		
		System.out.printf("\n|%15d| |%15d|", n, n);
		
		System.out.printf("\n%f " , k);

		//"Das ist eine Beispielsatz. Ein Beispielsatz ist das:"
		System.out.println("\n \"Das ist ein Beispielsatz. \n Ein Beispielsatz ist das.\"");
		
		System.out.printf("\n%f" , r);
		
		System.out.printf("\n%f" , e);
		
		System.out.printf("\n%f" , l);
		
		System.out.printf("\n%f" , u);
		
		System.out.printf("\n%f" , p);
		
		String o = "*";
		String a = "***";
		String b = "*****";
		String c = "*******";
		String d = "*********";
		String f = "***********";
		String g = "*************";
		String h = "***";
		String j = "***";
		
		System.out.printf("\n %20s\n ", o );
		System.out.printf("%21s\n", a );
		System.out.printf("%23s\n", b);
		System.out.printf("%24s\n", c);
		System.out.printf("%25s\n", d);
		System.out.printf("%26s\n", f);
		System.out.printf("%27s\n", g);
		System.out.printf("%22s\n", h);
		System.out.printf("%22s\n", j);
	}

}
