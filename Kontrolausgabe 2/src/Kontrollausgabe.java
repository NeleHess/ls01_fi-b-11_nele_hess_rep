
public class Kontrollausgabe {

	public static void main(String[] args) {
		
		String one = "Fahrenheit";
		String two = "-20";
		String three = "-10";
		String four = "0";
		String five = "20";
		String six = "30";
		
		
		System.out.printf("%-2s |  Celsisus\n", one);
		System.out.printf("-----------------------\n");
		System.out.printf("%-10s |    -28.88\n", two);
		System.out.printf("%-10s |    -23.33\n", three);
		System.out.printf("%-10s |    -17.77\n", four);
		System.out.printf("%-10s |     -6.66 \n", five);
		System.out.printf("%-10s |     -1.11 \n",six);
		
		
		
	}

}
